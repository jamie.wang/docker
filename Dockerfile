FROM docker:edge-git

RUN apk add --no-cache py2-pip
RUN pip install --upgrade pip && pip install docker-compose
